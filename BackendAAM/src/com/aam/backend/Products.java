package com.aam.backend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aam.backend.dto.Product;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Path("/products")
public class Products {

	private static Logger logger = Logger.getLogger(Products.class.getSimpleName());

	private static String FILEPATH = "/home/wildfly/products.json";

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProducts(@QueryParam("term") String term, @QueryParam("limit") int limit) {
		logger.info("term " + term + " limit " + limit);
		JsonObject r = readFromFile();
		if (r != null) {
			if (term != null && !term.equals("")) {
				JsonArray jArrayResp = new JsonArray();
				JsonObject rResp = new JsonObject();
				rResp.add("products", jArrayResp);

				JsonArray jArray = r.get("products").getAsJsonArray();
				Iterator<JsonElement> it = jArray.iterator();
				int n = 0;
				while (it.hasNext() && n < limit) {
					JsonElement element = it.next();
					if (element.getAsJsonObject().get("name").getAsString().toLowerCase().contains(term.toLowerCase())) {
						jArrayResp.add(element);
						n++;
					}
				}
				logger.info(rResp.toString());
				return Response.ok(new Gson().toJson(rResp), MediaType.APPLICATION_JSON).build();
			}
			return Response.ok(new Gson().toJson(r), MediaType.APPLICATION_JSON).build();
		}
		return Response.serverError().build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/product/{id}")
	public Response getProductById(@PathParam("id") String id) {
		logger.info("id " + id);
		JsonObject r = readFromFile();
		if (r != null) {
			if (id != null && !id.equals("")) {
				JsonArray jArray = r.get("products").getAsJsonArray();
				boolean found = false;
				Iterator<JsonElement> it = jArray.iterator();
				while (it.hasNext() && !found) {
					JsonElement element = it.next();
					logger.info(element.toString());
					if (element.getAsJsonObject().get("id").getAsString().equals(id)) {
						return Response.ok(new Gson().toJson(element), MediaType.APPLICATION_JSON).build();
					}
				}
			}

		}
		return Response.serverError().build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(String json) {
		logger.info("PUT " + json);
		try {
			Product updatedProduct = new Gson().fromJson(json, Product.class);
			JsonObject r = readFromFile();
			if (r != null) {
				JsonArray jArray = r.remove("products").getAsJsonArray();
				boolean found = false;
				Iterator<JsonElement> it = jArray.iterator();
				while (it.hasNext() && !found) {
					JsonElement element = it.next();
					logger.info(element.toString());
					if (element.getAsJsonObject().get("id").getAsString().equals(updatedProduct.getId())) {
						found = true;
						it.remove();
					}
				}
				if (found) {
					jArray.add(new Gson().toJsonTree(updatedProduct));
					r.add("products", jArray);
					if (saveToFile(r)) {
						return Response.ok(new Gson().toJson("success"), MediaType.APPLICATION_JSON).build();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.serverError().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProduct(String json) {
		logger.info("POST " + json);
		try {
			JsonObject r = readFromFile();
			if (r != null) {
				Product newProduct = new Gson().fromJson(json, Product.class);
				newProduct.setId(null);
				JsonArray jArray = r.remove("products").getAsJsonArray();
				Iterator<JsonElement> it = jArray.iterator();
				Integer newId = null;
				while (it.hasNext()) {
					JsonElement element = it.next();
					Integer elementId = Integer.parseInt(element.getAsJsonObject().get("id").getAsString());
					if (newId == null || newId.compareTo(elementId) < 0) {
						newId = elementId;
					}
				}
				newProduct.setId(newId == null ? "1" : (++newId).toString());
				newProduct.completeFieldsWithEmptyString();
				logger.info(newProduct.toString());
				jArray.add(new Gson().toJsonTree(newProduct));
				r.add("products", jArray);
				if (saveToFile(r)) {
					return Response.ok(new Gson().toJson("success"), MediaType.APPLICATION_JSON).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.serverError().build();
	}

	private JsonObject readFromFile() {
		FileReader fr = null;
		Gson gson = new Gson();
		try {
			fr = new FileReader(new File(FILEPATH));
			JsonObject r = gson.fromJson(fr, JsonObject.class);
			return r;
		} catch (FileNotFoundException e) {
			try {
				JsonArray jArray = new JsonArray();
				JsonObject r = new JsonObject();
				r.add("products", jArray);

				FileWriter fw;
				fw = new FileWriter(new File(FILEPATH));
				gson.toJson(r, fw);
				fw.flush();
				fw.close();
				return r;
			} catch (IOException e1) {
				e1.printStackTrace();
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (fr != null)
					fr.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean saveToFile(JsonObject jObj) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(new File(FILEPATH));
			new Gson().toJson(jObj, fw);
			fw.flush();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (fw != null)
					fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
