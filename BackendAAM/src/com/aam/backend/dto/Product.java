package com.aam.backend.dto;

public class Product {

	private String	id;
	private float	price;
	private String	name, description;
	private String	imageURL;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public void completeFieldsWithEmptyString() {
		if (imageURL == null)
			setImageURL("");
		if (description == null)
			setDescription("");
		if (name == null)
			setName("");
	}

	@Override
	public String toString() {
		return "[id=" + id + ", price=" + price + ", name=" + name + ", description=" + description + ", imageURL=" + imageURL + "]";
	}

}
