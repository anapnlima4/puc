package com.aam.backend;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class API extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	public API() {
		singletons.add(new Products());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
