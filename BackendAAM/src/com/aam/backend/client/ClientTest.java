package com.aam.backend.client;

import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aam.backend.dto.Product;
import com.google.gson.Gson;

public class ClientTest {
	
	private static Logger logger = Logger.getLogger(ClientTest.class.getSimpleName());

	private WebTarget baseTarget;

	public ClientTest(String serviceUrl) {
		Client client = ClientBuilder.newClient();
		baseTarget = client.target(serviceUrl);
	}

	public Response createProduct() {
		Invocation.Builder invocationBuilder = baseTarget.request(MediaType.APPLICATION_JSON);
		Product product = new Product();
		product.setId("1");
		product.setDescription("First product to test bla bla bla");
		product.setPrice(102.60f);
		product.setImageURL("https://??");
		Response response = invocationBuilder.post(Entity.entity(new Gson().toJson(product), MediaType.APPLICATION_JSON));
		if (response.getStatus() != Response.Status.OK.getStatusCode()) {
			logger.severe("Erro incluindo produto");
			product.setId("2");
			product.setDescription("Second product trying");
			product.setPrice(1002.60f);
			product.setImageURL("https://??");
			return invocationBuilder.post(Entity.entity(new Gson().toJson(product), MediaType.APPLICATION_JSON));
		}
		return response;
	}
	
	public Response updateProduct() {
		Invocation.Builder invocationBuilder = baseTarget.request(MediaType.APPLICATION_JSON);
		Product product = new Product();
		product.setId("1");
		product.setDescription("First product tested");
		product.setPrice(122.60f);
		product.setImageURL("https://??");
		Response response = invocationBuilder.put(Entity.entity(new Gson().toJson(product), MediaType.APPLICATION_JSON));
		if (response.getStatus() != Response.Status.OK.getStatusCode()) {
			logger.severe("Erro atualizando produto");
		}
		return response;
	}
}
